Used plugin - https://github.com/blueimp/jQuery-File-Upload

## TODO

* **on PHP side**: header 'Access-Control-Allow-Origin' is missing.

  For QA and Stage '*' can be ok, for prod it should be production domain (aut-prod.alphagraphics.com for testing + publisher domains 'alphagraphics.com' and PREVIEW which is not done now, so now domain here)
* Flash uploading is missing.


For testing I've used another domain to upload images to. This can be changes in 'js/main.js'*;
Project contains example PHP project which fully support this plugin (however we need only POST functionality, I think)