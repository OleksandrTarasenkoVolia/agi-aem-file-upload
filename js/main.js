
$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        // url: 'http://jquery-file-upload.appspot.com/',  //JUST FOR TESTING
        autoUpload: true,
        // url: 'https://fxtransport.alphagraphics.com/upload.php',    //PROD
        url: 'http://fxtransport.ag.voliasoftware.com/upload.php',    //TEST
        maxFileSize: 1999000,
        sequentialUploads: true,
        dataType: 'html'
    });

    //Wrap default version of add command to increment PHP_SESSION_UPLOAD_PROGRESS
    //We will need this to get file ids, for creating link
    var widgetInstance = $('#fileupload').data('blueimp-fileupload') || $('#fileupload').data('fileupload');
    widgetInstance.options.defAddFn = widgetInstance.options.add;
    widgetInstance.options.add = function myCustomFn() {

        console.log("add");
        $('#ses_prog').val(parseInt($('#ses_prog').val()) + 1);
        widgetInstance.options.defAddFn.apply(this, arguments);
    };

    //todo: Wrap 'done' callback, extract files ids from the server, create valid JSON and pass it to the original callback
    //https://fxtransport.alphagraphics.com/show.php?PHP_SESSION_UPLOAD_PROGRESS=NUMBER&SESSID=SESSION_ID
});
